from pad_650.ordem_decrescente import ordem_decrescente
import unittest

class TestOrdemDecrescente(unittest.TestCase):
    def test_ordem_decrescente(self):
        self.assertEqual(ordem_decrescente(1234556789), 9876554321)
        self.assertEqual(ordem_decrescente(9), 9)
        self.assertEqual(ordem_decrescente('abc'), 'error')
        self.assertEqual(ordem_decrescente([]), "error")
        self.assertEqual(ordem_decrescente(()), "error")
        self.assertEqual(ordem_decrescente({}), "error")
        self.assertEqual(ordem_decrescente(1.546), "error")
        self.assertEqual(ordem_decrescente(-65456), "error")