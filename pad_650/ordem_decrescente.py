def ordem_decrescente(numero) -> int and str:
    numero = list(str(numero))
    numero.sort(reverse=True)
    final = ''
    for i in numero:
        final += i
    try:
        return int(final)
    except:
        return 'error'
