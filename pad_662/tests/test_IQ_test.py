import unittest
from pad_662.IQ_test import qi_test

class TestQi(unittest.TestCase):

    def test_qi_test(self):
        self.assertEqual(qi_test("2 4 7 8 10"), 3)
        self.assertEqual(qi_test("1 3 6 5 9"), 3)
        self.assertEqual(qi_test("2 3 7 8 10"), "error")
        self.assertEqual(qi_test("23211"), "error")
        self.assertEqual(qi_test("a b e c d"), "error")
        self.assertEqual(qi_test(()), "error")
        self.assertEqual(qi_test({}), "error")
        self.assertEqual(qi_test(15464), "error")
        self.assertEqual(qi_test("@#"), "error")
