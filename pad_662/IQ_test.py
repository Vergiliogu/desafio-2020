def qi_test(numeros:str) -> str and int:
    try:
        numeros = numeros.split()
        pares = []
        impares = []
        for i in numeros:
            if int(i) % 2 == 0:
                pares.append(i)
            elif int(i) % 2 == 1:
                impares.append(i)

    except:
        return "error"

    if len(impares) == 1 and len(pares) > 1:
        diferente = impares[0]
    elif len(pares) == 1 and len(impares) > 1:
        diferente = pares[0]
    else:
        return "error"

    return numeros.index(diferente) + 1
