from pad_651.square_every import *
import unittest

class TestSquareEvery(unittest.TestCase):

    def test_square_every(self):
        self.assertEqual(quadrado_numero(6), 36)
        self.assertEqual(quadrado_numero(6655), 36362525)
        self.assertEqual(quadrado_numero(0), 0)
        self.assertEqual(quadrado_numero(1.65876), "error")
        self.assertEqual(quadrado_numero([]), "error")
        self.assertEqual(quadrado_numero(()), "error")
        self.assertEqual(quadrado_numero({}), "error")
        self.assertEqual(quadrado_numero("Teste string"), "error")