def quadrado_numero(numero) -> int and str:

    final = ""
    if isinstance(numero, int):
        for i in str(numero):
            final += str(int(i)**2)
        return int(final)
    else:
        return "error"
