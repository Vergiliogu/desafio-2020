from pad_653.planta_em_crescimento import *
import unittest

class TestCrescimento(unittest.TestCase):

    def test_crescimento(self):
        self.assertEqual(crescimento(10, 9, 4), 1)
        self.assertEqual(crescimento(100, 10, 910), 10)
        self.assertEqual(crescimento(4, 3, 100), "error")
        self.assertEqual(crescimento("5", 3, 100), "error")
        self.assertEqual(crescimento((), 3, 100), "error")
        self.assertEqual(crescimento(2.5, 3, 100), "error")

    def test_regras(self):
        self.assertEqual(regras(4, 3, 100), False)
        self.assertEqual(regras(10, 1, 100), False)
        self.assertEqual(regras(10, 5, 20000), False)
        self.assertEqual(regras(6, 3, 120), True)

