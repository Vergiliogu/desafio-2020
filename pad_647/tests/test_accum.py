from pad_647.accum import accum
import unittest

class TestResmungando(unittest.TestCase):

    def test_accum(self):
        self.assertEqual(accum("nnYt"), "N-Nn-Yyy-Tttt")
        self.assertEqual(accum("ABCD"), "A-Bb-Ccc-Dddd")
        self.assertEqual(accum("abcd"), "A-Bb-Ccc-Dddd")