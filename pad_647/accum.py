def accum(string:str) -> str:
    final = ""
    contador = 1
    for i in string:
        nova_str = i*contador
        final += f"{nova_str.title()}-"
        contador += 1
    final = final[:-1]
    return final