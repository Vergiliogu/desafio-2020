
def ordenar(matriz:list) -> list:

    numeros = []
    strings = []

    for i in matriz:

        if isinstance(i, float) or isinstance(i, int):
            numeros.append(i)
        elif isinstance(i, str):
            strings.append(i)

    numeros.sort()
    strings.sort()
    final = numeros + strings

    return final