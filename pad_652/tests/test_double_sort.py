import unittest
from pad_652.double_sort import ordenar

class TestOrdenar(unittest.TestCase):

    def test_ordenar(self):
        self.assertEqual(ordenar(["Banana", "Orange", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "Apple", "Banana", "Mango", "Orange"])
        self.assertEqual(ordenar(["154584", 0, 2, 2]), [0, 2, 2, "154584"])
        self.assertEqual(ordenar([0, 2, 2, 1.3]), [0, 1.3, 2, 2])
        self.assertEqual(ordenar(["Banana", "@#", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "@#", "Apple", "Banana", "Mango"])
        self.assertEqual(ordenar(["Banana", "@#", "3221", "Apple", "Mango", 0, 2, 2]), [0, 2, 2, "3221", "@#", "Apple", "Banana", "Mango"])