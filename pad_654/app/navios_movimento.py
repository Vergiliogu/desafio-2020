import math

class NaviosMovimento:

    def __init__(self, grau_1, grau_2):
        if (grau_1 or grau_2) > 359 or (grau_1 or grau_2) < 0:
            raise Exception("O número é muito grande")
        else:
            self.grau_1 = grau_1
            self.grau_2 = grau_2


    def conversion(self, angulos:list) -> list:
        angulos[0] = math.sin(math.radians(angulos[0]))
        angulos[1] = math.sin(math.radians(angulos[1]))
        angulos[2] = math.sin(math.radians(angulos[2]))

        return angulos


    def calculo_distancia(self):
        angulos = self.calculo_angulo()
        angulos = self.conversion(angulos)
        x = angulos[1] * (64 / angulos[0])
        return x


    def execoes(self):
        if self.grau_1 > 180:
            oposto = self.grau_1 - 180
        else:
            oposto = 180 - self.grau_1

        if self.grau_1 == self.grau_2:
            return "A rede nunca vai rasgar"
        elif self.grau_2 == oposto:
            return self.calculo_com_angulos_opostos()
        else:
            return False


    def calculo_com_angulos_opostos(self):
        tempo = (32 / 150) * 60
        return tempo


    def calculo_tempo(self):
        x = self.calculo_distancia()
        final = (x / 150) * 60
        return "{:.2f} minutos" .format(final)


    def calculo_angulo(self):
        if self.grau_1 > (180 + self.grau_2):
            angulo = 360 - self.grau_1 + self.grau_2
        elif self.grau_1 > self.grau_2:
            angulo = self.grau_1 - self.grau_2
        else:
            angulo = self.grau_2 - self.grau_1
        angulos_iguais = (180 - angulo) / 2
        lista = [angulo, angulos_iguais, angulos_iguais]
        return lista

