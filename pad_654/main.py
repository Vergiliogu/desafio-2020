from pad_654.bootstrap import Startup

print('*' * 40)

app = Startup()
app.execute()

print('*' * 40)