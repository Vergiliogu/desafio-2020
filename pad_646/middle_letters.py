def get_middle_letters(palavra:str) -> str:
    if isinstance(palavra, str):
        posicao = (len(palavra) // 2)
        if len(palavra) % 2 == 0:
            return f"{palavra[posicao - 1]}{palavra[posicao]}"
        else:
            return f"{palavra[posicao]}"
    else:
        return "error"
