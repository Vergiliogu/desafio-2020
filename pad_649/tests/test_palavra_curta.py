from pad_649.palavra_curta import *
import unittest

class TestPalavraCurta(unittest.TestCase):

    def test_palavra_curta(self):
        self.assertEqual(menor("bitcoin take over the world maybe who knows perhaps"), 3)
        self.assertIsInstance(menor("bitcoin take over the world maybe who knows perhaps"), int)
