def menor(frase:str) -> int:
    lista_frase = frase.split()
    menor = '*'*99999999
    for i in lista_frase:
        if len(i) < len(menor):
            menor = i

    return len(menor)

