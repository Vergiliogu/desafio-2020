def vowel_count(phrase:str) -> int:
    vowel = 'aeiouáàãâäéèêëíìîïóòõôöúùûü'
    total = 0
    for i in vowel:
        total += phrase.lower().count(i)

    return total
